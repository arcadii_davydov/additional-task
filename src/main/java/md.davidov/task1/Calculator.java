package md.davidov.task1;

public class Calculator {

    public static double multiply(double firstNumber, double secondNumber) {
        if (0 <= firstNumber && firstNumber < 100 && 0 <= secondNumber && secondNumber < 100) { //to clarify <100 | <=100
            return firstNumber * secondNumber;
        }
        return -1;
    }

    public static int substract(int firstNumber, int secondNumber) {
        if (0 <= secondNumber && firstNumber >= secondNumber) {
            return firstNumber - secondNumber;
        }
        return -1;
    }

    public static double divide(double numberToBeDivided, double divisor) {
        if (0 <= divisor && numberToBeDivided >= divisor) {
            return numberToBeDivided / divisor;
        }
        return 0;
    }
}