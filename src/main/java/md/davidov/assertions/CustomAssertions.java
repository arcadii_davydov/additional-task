package md.davidov.assertions;

import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.lang.System.lineSeparator;

public class CustomAssertions {

    private static Logger logger = LoggerFactory.getLogger(CustomAssertions.class);
    private static final String ASSERT_THAT = "Assert that ";

    public static <T> void assertThat(final String message, final T actual, final Matcher<? super T> matcher) {
        final String fullMessage = ASSERT_THAT + message;
        final String logMessage = getAssertionExpectedActualMessage(fullMessage, valueOf(matcher), valueOf(actual));
        try {
            MatcherAssert.assertThat(fullMessage, actual, matcher);
            logger.info(logMessage);
        } catch (AssertionError error) {
            logger.error(logMessage);
            throw new AssertionError(error.getMessage());
        }
    }

    public static <T> void assertEquals(final String message, final T actual, final Matcher<? super T> matcher) {
        final String fullMessage = ASSERT_THAT + message;
        final String logMessage = getAssertionExpectedActualMessage(fullMessage, valueOf(matcher), valueOf(actual));
        try {
            MatcherAssert.assertThat(fullMessage, actual, matcher);
            logger.info(logMessage);
        } catch (AssertionError error) {
            logger.error(logMessage);
            throw new AssertionError(error.getMessage());
        }
    }

    public static String getAssertionExpectedActualMessage(final String fullMessage, final String expected, final String actual) {
        return format("%s%4$s - [ EXPECTED]: %s%4$s - [ ACTUAL  ]: %s", fullMessage, expected, actual, lineSeparator());
    }
}