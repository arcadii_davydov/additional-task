package md.davidov.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    public static WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public static void selectPageByTopic(String topic) {
        driver.findElement(By.cssSelector("#searchInput")).sendKeys(topic);
        driver.findElement(By.cssSelector("#searchButton")).click();
        new SelectedPage(driver);
    }
}