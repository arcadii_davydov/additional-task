package md.davidov.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SelectedPage {
    public static WebDriver driver;

    public SelectedPage(WebDriver driver) {
        this.driver = driver;
    }

    public static int countSpecificWord(String word) {
        int count = 0;
        String webPageText = driver.findElement(By.cssSelector("*")).getText();
        Pattern p = Pattern.compile("(?i)\\b" + word + "\\b");
        Matcher m = p.matcher(webPageText);
        while (m.find()) {
            count++;
        }
        return count;
    }
}