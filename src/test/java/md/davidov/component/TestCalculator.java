package md.davidov.component;

import md.davidov.task1.Calculator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static md.davidov.assertions.CustomAssertions.assertEquals;
import static md.davidov.assertions.CustomAssertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class TestCalculator {

    @ParameterizedTest
    @DisplayName("Testing Multiplication")
    @CsvSource(value = {"5.2, 25, 130", "85, 16 ,1360", "100, 7, -1", "16, 100, -1", "81, -12, -1", "-5, 18, -1", "-0.00000001, 51, -1", "33, -0.00000001, -1"})
    public void testMultiplication(double firstNumber, double secondNumber, double expectedResult) {
        assertAll(
                () -> assertThat("Verifying multiplication", Calculator.multiply(firstNumber, secondNumber), equalTo(expectedResult))
//                () -> assertThrows(NullPointerException.class, () -> Calculator.multiply(81.0, null))
        );
    }

    @ParameterizedTest
    @DisplayName("Testing Substraction")
    @CsvFileSource(files = {"src/test/resources/params/NumbersForTestSubstraction.csv",
            "src/test/resources/params/NumbersForTestSubstraction2.csv"},
            numLinesToSkip = 1)
    public void testSubstraction(int firstNumber, int secondNumber, int expectedResult) {
        assertEquals("Verifying subtraction", Calculator.substract(firstNumber, secondNumber), equalTo(expectedResult));
    }

    @ParameterizedTest
    @DisplayName("Testing Division")
    @MethodSource("provideNumbersForTestDivision")
    public void testDivision(double firstNumber, double secondNumber, double expectedResult) {
        assertAll(
                () -> assertEquals(expectedResult, Calculator.divide(firstNumber, secondNumber), 0.00001, "Result of division is wrong"),
                () -> assertThrows(NumberFormatException.class, () -> Calculator.divide(81.0, Double.parseDouble("rt")))
        );
    }

    private static Stream<Arguments> provideNumbersForTestDivision() {
        return Stream.of(
                arguments(16984.7, 350.2, 48.5),
                arguments(58.5, 4.5, 13.0),
                arguments(5.2, 25.0, 0),
                arguments(-0.0001, 18.0, 0),
                arguments(72.0, -0.0001, 0),
                arguments(22.0, 0, "Infinity")
        );
    }
}