package junit;

import base.DriverManager;
import md.davidov.pages.HomePage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.io.IOException;

import static actions.TestType.selectTestType;

public class CountWordsTest extends DriverManager {

    @Test
    public void testCountCertainWordsByTopicsInWikipedia() throws IOException {
        selectTestType();
    }

    @AfterClass
    public void tearDown() {
        DriverManager.quit();
    }

    public static void setUp(String url) {
        DriverManager.initialize();
        new HomePage(driver);
        driver.get(url);
    }
}