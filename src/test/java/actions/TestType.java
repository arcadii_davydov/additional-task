package actions;

import java.io.IOException;
import java.util.Scanner;

import static actions.OperationsWithDataFromConsole.testWithDataFromConsole;
import static actions.OperationsWithDataFromExcel.readAndWriteDataInExcel;
import static junit.CountWordsTest.setUp;
import static md.davidov.utils.TestUtils.TEST_FILE_PATH;
import static md.davidov.utils.TestUtils.WIKIPEDIA_URL;

public class TestType {

    public static void selectTestType() throws IOException {
        Scanner in = new Scanner(System.in);
        String typeOfTest;
        while (true) {
            System.out.println("Please press \"1\" for test with .xlsx file or press \"2\" for test in console :");
            typeOfTest = in.nextLine();
            if (!(typeOfTest.equals("1") || (typeOfTest.equals("2")))) {
                System.out.println("The entered value is invalid!");
            } else
                break;
        }

        if (typeOfTest.equals("1")) {
            setUp(WIKIPEDIA_URL);
            readAndWriteDataInExcel(TEST_FILE_PATH);
            System.out.println("Test is done! Check results in .xlsx file please!");
        }
        if (typeOfTest.equals("2")) {
            testWithDataFromConsole();
        }
    }
}