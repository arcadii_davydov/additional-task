package actions;

import md.davidov.pages.HomePage;
import md.davidov.pages.SelectedPage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperationsWithDataFromExcel {
    public static void readAndWriteDataInExcel(String filePath) throws IOException {

        FileInputStream file = new FileInputStream(filePath);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Map<Integer, List<String>> data = new HashMap<>();
        int i = 0;
        for (Row row : sheet) {
            data.put(i, new ArrayList<>());
            for (Cell cell : row) {
                if (String.valueOf(cell).equals("")) {
                    break;
                }
                data.get(i).add(String.valueOf(cell));
            }
            i++;
        }

        List<String> topics = data.get(0);
        int numberOfTopics = topics.size();
        for (int j = 0; j < numberOfTopics; j++) {
            String topic = topics.get(j);
            HomePage.selectPageByTopic(topic);
            for (int k = 1; k <= sheet.getLastRowNum(); k++) {
                String word = data.get(k).get(0);//
                int count = SelectedPage.countSpecificWord(word);

                try {
                    XSSFCell cell;
                    XSSFCellStyle horizontalCenter = workbook.createCellStyle();
                    horizontalCenter.setAlignment(HorizontalAlignment.CENTER);
                    XSSFRow sheetrow = sheet.getRow(k);

                    if (sheetrow == null) {
                        sheetrow = sheet.createRow(k);
                    }

                    cell = sheetrow.getCell(j + 1);
                    if (cell == null) {
                        cell = sheetrow.createCell(j + 1);
                    }
                    if (count == 0) {
                        cell.setCellValue("no matches found");
                        sheet.autoSizeColumn(j + 1);
                    } else {
                        cell.setCellValue(count);
                    }
                    cell.setCellStyle(horizontalCenter);

                    FileOutputStream outFile = new FileOutputStream(filePath);
                    workbook.write(outFile);
                    outFile.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}