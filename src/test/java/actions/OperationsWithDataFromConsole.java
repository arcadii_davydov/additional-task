package actions;

import com.bethecoder.ascii_table.ASCIITable;
import md.davidov.pages.HomePage;
import md.davidov.pages.SelectedPage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static base.DriverManager.driver;
import static junit.CountWordsTest.setUp;
import static md.davidov.utils.TestUtils.WIKIPEDIA_URL;

public class OperationsWithDataFromConsole {
    public static void testWithDataFromConsole() {

        Scanner in = new Scanner(System.in);
        System.out.print("Please enter topics in Wikipedia:");
        String stringWithTopics = in.nextLine();

        List<String> topics = Arrays.asList(stringWithTopics.split(","));
        List<String> topicsWithEmptyFirstElement = new ArrayList<>();
        topicsWithEmptyFirstElement.add(0, "");
        topicsWithEmptyFirstElement.addAll(topics);


        System.out.print("Please enter search words:");
        String stringWithWords = in.nextLine();
        List<String> words = Arrays.asList(stringWithWords.split(","));

        setUp(WIKIPEDIA_URL);
        List<String> listOfCounts = sendDataForCounting(topics, words);
        driver.close();
        String[] tableHeader = new String[topicsWithEmptyFirstElement.size()];
        topicsWithEmptyFirstElement.toArray(tableHeader);
        List<String> dataForTable = prepareDataforTable(topics, words, listOfCounts);
        String[][] arrayWithDataForTable = convertDataForTableToTwoDimensionalArray(topics,words,dataForTable);

        System.out.println("\n\n Results table below:");
        ASCIITable.getInstance().printTable(tableHeader, 0, arrayWithDataForTable, 0);
    }

    public static List<String> sendDataForCounting(List<String> topics, List<String> words) {
        List<String> listOfCounts = new ArrayList<>();
        for (String topic : topics) {
            HomePage.selectPageByTopic(topic);
            for (String s : words) {
                String word = s.trim();
                int count = SelectedPage.countSpecificWord(word);
                String counts;
                if (count == 0) {
                    counts = ("no matches found");
                } else counts = String.valueOf(count);
                listOfCounts.add((counts));
            }
        }
        return listOfCounts;
    }

    public static List<String> prepareDataforTable(List<String> topics, List<String> words, List<String> listOfCounts) {
        List<String> dataForTable = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            dataForTable.add(words.get(i));
            for (int j = 0; j < topics.size(); j++) {
                int nr = j * words.size() + i;
                if (nr < listOfCounts.size()) {
                    dataForTable.add(listOfCounts.get(nr));
                }
            }
        }
        return dataForTable;
    }

    public static String[][] convertDataForTableToTwoDimensionalArray(List<String> topics, List<String> words, List<String> dataForTable) {
        int numberOfCount = 0;
        String[][] arrayWithDataForTable = new String[words.size()][topics.size() + 1];
        for (int i = 0; i < words.size(); i++) {
            for (int j = 0; j < topics.size() + 1; j++) {
                arrayWithDataForTable[i][j] = dataForTable.get(numberOfCount);
                numberOfCount++;
            }
        }
        return arrayWithDataForTable;
    }
}